package logger;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class loggingCR {

    private static final Logger log = LogManager.getLogger(loggingCR.class);

    public void createLog(String type, String message) {
        PropertyConfigurator.configure("src\\resources\\log4j.properties");

        if (type.equalsIgnoreCase("debug")) {
            log.debug(message);
        }
        if (type.equalsIgnoreCase("info")) {
            log.info(message);
        }
        if (type.equalsIgnoreCase("error")) {
            log.error(message);
        }
        if (type.equalsIgnoreCase("fatal")) {
            log.fatal(message);
        }
    }
}
