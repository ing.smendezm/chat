package Model;

import javax.swing.ImageIcon;
import java.io.Serializable;

public class Mensajes implements Serializable {

    private String type;
    private String path;
    private String name;
    private String text;
    private byte[] data;
    private ImageIcon icon;

    public Mensajes() {
    }

    public Mensajes(String type, String path, String name, String text, byte[] data, ImageIcon icon) {
        this.type = type;
        this.path = path;
        this.name = name;
        this.text = text;
        this.data = data;
        this.icon = icon;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public ImageIcon getIcon() {
        return icon;
    }

    public void setIcon(ImageIcon icon) {
        this.icon = icon;
    }

}
