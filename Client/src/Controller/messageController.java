package Controller;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import javax.swing.ImageIcon;
import logger.logging;
import Model.Mensajes;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class messageController {

    private ObjectInputStream input;
    private Socket connection;
    private final int port = 6789;
    private ObjectOutputStream output;
    private final logging log = new logging();
    private final Mensajes ms = new Mensajes();

    public Boolean startRunning(String user) {
        try {
            connection = new Socket("127.0.0.1", port);
            output = new ObjectOutputStream(connection.getOutputStream());
            input = new ObjectInputStream(connection.getInputStream());
            ms.setName(user);
            log.createLog("info", "Conexion Exitosa");
            return true;
        } catch (IOException e) {
            log.createLog("error", "Error Intentando Conexion: " + e.getMessage());
            return false;
        }
    }

    public void sendMessage(String mss) {
        try {
            cleanVariables();
            ms.setType("mss");
            ms.setText(mss);
            output.writeObject(ms);
            output.flush();
            log.createLog("info", "Enviando Mensaje");
        } catch (IOException e) {
            log.createLog("error", "Error enviando Mensaje: " + e.getMessage());
        }

    }

    public void sendPhoto(ImageIcon image) {
        cleanVariables();
        try {
            ms.setType("pic");
            ms.setIcon(image);
            output.writeObject(ms);
            output.flush();
            log.createLog("info", "Enviando Imagen");
        } catch (IOException e) {
            log.createLog("error", "Error enviando imagen: " + e.getMessage());
        }

    }

    public void sendIcon(ImageIcon image) {
        try {
            cleanVariables();
            ms.setType("icon");
            ms.setIcon(image);
            output.writeObject(ms);
            output.flush();
            log.createLog("info", "Enviando Icono");
        } catch (IOException e) {
            log.createLog("error", "Error enviando Icono: " + e.getMessage());
        }

    }

    public void sendFiles(File file) {
        try {
            cleanVariables();
            FileInputStream in = new FileInputStream(file);
            byte data[] = new byte[in.available()];
            in.read(data);
            in.close();
            ms.setType("file");
            ms.setData(data);
            output.writeObject(ms);
            output.flush();
            log.createLog("info", "Enviando Archivo");
        } catch (IOException e) {
            log.createLog("error", "Error enviando archivos: " + e.getMessage());
        }
    }

    public ObjectInputStream getInput() {
        return input;
    }

    public void getFile(Mensajes ms, String path) {
        try {
            File file = new File(ms.getName());
            String auxPath = path + "\\" + file.getName();
            FileOutputStream out = new FileOutputStream(auxPath);
            out.write(ms.getData());
            System.out.println("Ruta de guardado: " + auxPath);
            out.close();

        } catch (IOException e) {
            System.out.println("Nombre del archivo :" + ms.getName());
            log.createLog("error", "Error obteniendo archivo: " + e.getMessage());
        }
    }

    private void cleanVariables() {
        ms.setData(null);
        ms.setIcon(null);
        ms.setPath(null);
        ms.setText(null);
        ms.setType(null);
    }
}
