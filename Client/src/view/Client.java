package view;

import Controller.messageController;
import Model.Mensajes;
import java.awt.Adjustable;
import java.awt.Color;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.io.IOException;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import logger.logging;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JTextField;
import javax.swing.JLabel;

public class Client extends javax.swing.JFrame {

    private messageController ms = new messageController();
    private Thread th;
    private String name;
    private String path = "";
    private String pathAux = "";
    private logging log = new logging();

    public Client() {

        initComponents();
        this.setTitle("Cliente");
        this.setVisible(true);
        txtStatus.setText("Desconectado");
        txtStatus.setBackground(Color.red);
        paths();

    }

    @SuppressWarnings("unchecked")

    public void startConn(String user) {
        if (ms.startRunning(user)) {
            lblUser.setText(user);
            txtStatus.setText("Conectado");
            txtStatus.setBackground(Color.GREEN);
            name = user;
            th = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        while (true) {
                            Mensajes ms1 = (Mensajes) ms.getInput().readObject();

                            String type = ms1.getType();

                            if (type.equals("mss")) {

                                printMessage(ms1.getName(), ms1.getText());

                            }
                            if (type.equals("pic")) {

                                System.out.println("Llego imagen");
                                printImage(ms1.getIcon());

                            }
                            if (type.equals("file")) {

                                ms.getFile(ms1, pathAux);
                                printMessage(ms1.getName(), "Archivo: " + ms1.getPath());

                            }
                            if (type.equals("icon")) {

                            }
                        }
                    } catch (ClassNotFoundException e) {
                    } catch (IOException ex) {

                    }

                }
            });
            th.start();
        }
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txtMessage = new javax.swing.JTextField();
        btnSend = new javax.swing.JButton();
        status = new javax.swing.JLabel();
        jSendFiles = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        txtStatus = new javax.swing.JTextField();
        btnPath = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        lblPath = new javax.swing.JLabel();
        lblUser = new javax.swing.JLabel();
        scrollChat = new javax.swing.JScrollPane();
        panelChat = new javax.swing.JDesktopPane(){
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.setColor(Color.WHITE);
                g.fillRect(0, 0, getWidth(), getHeight());
            }
        };
        btnSendImage = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanel1.setLayout(null);
        jPanel1.add(txtMessage);
        txtMessage.setBounds(30, 60, 270, 30);

        btnSend.setText("Enviar");
        btnSend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSendActionPerformed(evt);
            }
        });
        jPanel1.add(btnSend);
        btnSend.setBounds(310, 60, 80, 30);

        status.setText("Escriba Aqui");
        jPanel1.add(status);
        status.setBounds(30, 90, 110, 20);

        jSendFiles.setText("Enviar Adjunto");
        jSendFiles.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jSendFilesActionPerformed(evt);
            }
        });
        jPanel1.add(jSendFiles);
        jSendFiles.setBounds(33, 390, 130, 30);

        jLabel3.setBackground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Usuario");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(30, 10, 100, 20);

        txtStatus.setEditable(false);
        jPanel1.add(txtStatus);
        txtStatus.setBounds(30, 30, 100, 20);

        btnPath.setText("Ruta");
        btnPath.setToolTipText("Seleccione la ruta para guardar archivos a recibir");
        btnPath.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPathActionPerformed(evt);
            }
        });
        jPanel1.add(btnPath);
        btnPath.setBounds(310, 10, 80, 23);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Ruta Seleccionada");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(40, 440, 110, 14);

        lblPath.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jPanel1.add(lblPath);
        lblPath.setBounds(170, 440, 230, 14);

        lblUser.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jPanel1.add(lblUser);
        lblUser.setBounds(80, 10, 130, 20);

        scrollChat.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        scrollChat.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        panelChat.setBackground(new java.awt.Color(255, 255, 255));
        panelChat.setLayout(new javax.swing.BoxLayout(panelChat, javax.swing.BoxLayout.Y_AXIS));
        scrollChat.setViewportView(panelChat);

        jPanel1.add(scrollChat);
        scrollChat.setBounds(30, 130, 350, 250);

        btnSendImage.setText("Enviar Imagen");
        btnSendImage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSendImageActionPerformed(evt);
            }
        });
        jPanel1.add(btnSendImage);
        btnSendImage.setBounds(190, 390, 120, 30);
        jPanel1.add(jLabel1);
        jLabel1.setBounds(0, 0, 420, 480);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 421, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE)
        );

        setSize(new java.awt.Dimension(437, 513));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    private void printMessage(String name, String mes) {
        String coml = "\n" + name + " Dice: " + mes;
        JTextField textField = new JTextField();
        textField.setSize(100, 30);
        textField.setEditable(false);
        textField.setBackground(Color.WHITE);
        textField.setText(coml);
        panelChat.add(textField);
        panelChat.revalidate();
        panelChat.repaint();
        scrollToBottom(scrollChat);
    }

    private void scrollToBottom(JScrollPane scrollPane) {

        final JScrollBar verticalBar = scrollPane.getVerticalScrollBar();
        AdjustmentListener downScroller = new AdjustmentListener() {
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                Adjustable adjustable = e.getAdjustable();
                adjustable.setValue(adjustable.getMaximum());
                verticalBar.removeAdjustmentListener(this);
            }
        };
        verticalBar.addAdjustmentListener(downScroller);
    }

    private Boolean validarCampo() {
        boolean result = (txtMessage.equals("") || txtMessage.equals(null)) ? false : true;
        return result;
    }

    private void btnSendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSendActionPerformed
        if (validarCampo()) {
            ms.sendMessage(txtMessage.getText());
            printMessage(name, txtMessage.getText());
            txtMessage.setText("");
        } else {
            JOptionPane.showMessageDialog(null, "Mensaje Vacio!!");
        }
    }//GEN-LAST:event_btnSendActionPerformed

    private void jSendFilesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jSendFilesActionPerformed
        JFileChooser chooser = new JFileChooser();
        chooser.addChoosableFileFilter(new FileNameExtensionFilter("Documentos", "pdf", "docx", "txt", "csv", "xls", "xlsx"));
        chooser.addChoosableFileFilter(new FileNameExtensionFilter("Audios", "wav", "mp3", "ogg"));
        chooser.addChoosableFileFilter(new FileNameExtensionFilter("Videos", "mp4", "mpeg"));
        int resul = chooser.showOpenDialog(null);
        if (resul == JFileChooser.APPROVE_OPTION) {
            ms.sendFiles(chooser.getSelectedFile());
            System.out.println(chooser.getSelectedFile());
        }
    }//GEN-LAST:event_jSendFilesActionPerformed

    private void btnPathActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPathActionPerformed
        paths();
    }//GEN-LAST:event_btnPathActionPerformed

    private void btnSendImageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSendImageActionPerformed
        JFileChooser chooser = new JFileChooser();
        chooser.addChoosableFileFilter(new FileNameExtensionFilter("Imagenes", "png", "jpg", "jpeg"));
        int resul = chooser.showOpenDialog(null);
        if (resul == JFileChooser.APPROVE_OPTION) {
            log.createLog("info", "Archivo seleccionado: " + chooser.getSelectedFile().getPath());
            ImageIcon image = new ImageIcon(chooser.getSelectedFile().getAbsolutePath());
            ms.sendPhoto(image);

        }
    }//GEN-LAST:event_btnSendImageActionPerformed

    private void printImage(ImageIcon img) {
        JLabel lblImg = new JLabel();
        ImageIcon img1 = transformImg(img);
        lblImg.setIcon(img1);
        panelChat.add(lblImg);
        panelChat.revalidate();
        panelChat.repaint();
    }

    private ImageIcon transformImg(ImageIcon img) {
        Image newImage = img.getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH);
        ImageIcon icon = new ImageIcon(newImage);
        return icon;
    }

    private String paths() {
        JFileChooser chooser = new JFileChooser();
        int respuesta;
        if (path.equals("") && pathAux.equals("")) {
            JOptionPane.showMessageDialog(this, "Seleccione una ruta valida \n"
                    + "para guardar los archivos \n"
                    + "a recibir");
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            respuesta = chooser.showSaveDialog(this);
            if (respuesta == JFileChooser.APPROVE_OPTION) {
                path = chooser.getSelectedFile().getPath();
                lblPath.setText(path);
                pathAux = path;
                log.createLog("info", "Ruta Seleccionada: " + path);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione una ruta valida \n"
                    + "para guardar los archivos \n"
                    + "a recibir");
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            respuesta = chooser.showSaveDialog(this);
            if (respuesta == JFileChooser.APPROVE_OPTION) {
                path = chooser.getSelectedFile().getPath();
                lblPath.setText(path);
                pathAux = path;
                log.createLog("info", "Ruta Seleccionada: " + path);
            }
        }
        return path;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnPath;
    private javax.swing.JButton btnSend;
    private javax.swing.JButton btnSendImage;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton jSendFiles;
    private javax.swing.JLabel lblPath;
    private javax.swing.JLabel lblUser;
    private javax.swing.JDesktopPane panelChat;
    private javax.swing.JScrollPane scrollChat;
    private javax.swing.JLabel status;
    private javax.swing.JTextField txtMessage;
    private javax.swing.JTextField txtStatus;
    // End of variables declaration//GEN-END:variables
}
